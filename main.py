# Программа создает базу данных людей с указанными полями, заполняет её, добавляет, редактирует и удаляет значения
# в строках и ячейках таблицы.

import sqlite3 as sql
from random import randint

def create_database(db_cursor):
    """Создание таблицы"""
    db_cursor.execute("""
        CREATE TABLE IF NOT EXISTS people(
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            first_name TEXT NOT NULL,
            last_name TEXT NOT NULL,
            sex TEXT NOT NULL,
            salary INTEGER,
            position TEXT,
            email TEXT,
            age INTEGER NOT NULL
        )
        """)

with sql.connect('main.db') as connection:
    cursor = connection.cursor()

    create_database(cursor)

    # Заполнение таблицы данными людей
    cursor.execute(f"""
        INSERT INTO people(first_name, last_name, sex, salary, position, email, age) VALUES(
            'Todd', 'Andrews', 'male', NULL, 'Manager', NULL, {randint(20, 50)} 
        ),(
            'Baldwin', 'Campbell', 'male', {randint(1000, 50000)}, 'Telemarketer', 'someemail@email.email', {randint(20, 50)}
        ),(
            'Hubert', 'Hines', 'male', {randint(1000, 50000)}, NULL, 'someemail@email.email', {randint(20, 50)}
        ),(
            'Mark', 'Waters', 'male', {randint(1000, 50000)}, 'Middle School Teacher', NULL, {randint(20, 50)}
        ),(
            'Paul', 'Booker', 'male', NULL, NULL, 'someemail@email.email', {randint(20, 50)}
        ),(
            'Daniel', 'Smith', 'male', {randint(1000, 50000)}, NULL, 'someemail@email.email', {randint(20, 50)}
        ),(
            'Charles', 'Daniels', 'male', NULL, 'Security Guard', 'someemail@email.email', {randint(20, 50)}
        ),(
            'Anthony', 'Gordon', 'male', {randint(1000, 50000)}, NULL, NULL, {randint(20, 50)}
        ),(
            'Matthew', 'Ryan', 'male', {randint(1000, 50000)}, NULL, 'someemail@email.email', {randint(20, 50)}
        ),(
            'Gerard', 'Cameron', 'male', NULL, NULL, NULL, {randint(20, 50)}
        ),(
            'Gerard', 'Cameron', 'female', NULL, 'Medical Assistant', NULL, {randint(20, 50)}
        ),(
            'Anne', 'Hall', 'female', NULL, 'Architect', NULL, {randint(20, 50)}
        ),(
            'Jasmine', 'Manning', 'female', {randint(1000, 50000)}, 'Clinical Laboratory Technician', NULL, {randint(20, 50)}
        ),(
            'Tamsin', 'Lester', 'female', {randint(1000, 50000)}, NULL, 'someemail@email.email', {randint(20, 50)}
        ),(
            'Brook', 'Rice', 'female', NULL, NULL, 'someemail@email.email', {randint(20, 50)}
        ),(
            'Camilla', 'Baldwin', 'female', NULL, 'HR Specialist', NULL, {randint(20, 50)}
        ),(
            'Emma', 'Morris', 'female', {randint(1000, 50000)}, 'Electrical Engineer', 'ssomeemail@email.email', {randint(20, 50)}
        ),(
            'Sophie', 'McCormick', 'female', NULL, 'Mathematician', NULL, {randint(20, 50)}
        ),(
            'Poppy', 'Fowler', 'female', NULL, NULL, NULL, {randint(20, 50)}
        ),(
            'Caroline', 'Sanders', 'female', NULL, 'Radiologic Technologist', NULL, {randint(20, 50)}
        ),(
            'Laurence', 'Wachowski', 'male', {randint(1000, 50000)}, 'Filmmaker', 'ssomeemail@email.email', 56
        ),(
            'Andrew', 'Wachowski', 'male', NULL, 'Screenwriter', NULL, 53
        );
    """)
    connection.commit()

    # Замена пола Wachowski на женский
    cursor.execute("""
        UPDATE people SET sex = 'female' WHERE last_name = 'Wachowski';
    """)

    # Замена имён Wachowski на Lana & Lilly
    cursor.execute("""
        UPDATE people SET first_name =
        CASE 
            WHEN first_name = 'Laurence' THEN 'Lana'
            WHEN first_name = 'Andrew' THEN 'Lilly'
        END
        WHERE last_name = 'Wachowski';
    """)
    connection.commit()

    # Генерация email вместо пустых значений
    cursor.execute(f"""
        UPDATE people SET email = LOWER(first_name) || '.' || LOWER(last_name) || '@email.com' WHERE email IS NULL;
    """)
    connection.commit()

    # Вывод данных людей с зарплатой больше 10 000
    high_salary = cursor.execute("""
        SELECT * FROM people WHERE salary > 10000;
    """)
    print('Salary is higher that 10 000')
    for row in high_salary:
        print(row)

    print('-'*100)

    # Вывод данных людей с зарплатой больше 10 000 и старше 25 лет
    high_salary_and_age = cursor.execute("""
        SELECT * FROM people WHERE salary > 10000 AND age > 25;
    """)
    print('Salary is higher than 10 000 and age is higher than 25')
    for row in high_salary_and_age:
        print(row)

    # Удаление данных всех людей без должности
    cursor.execute("""
        DELETE FROM people WHERE position IS NULL
    """)
    connection.commit()
    cursor.close()
